Zum Team gehören Sina Özcorapci und Lea Nora Ott-Hansen.
Die Aufgabenverteilung ist sehr einfach: Wir arbeiten weitgehend gemeinsam und
ergänzen uns gegenseitig. Allein einzelne Aufgaben, bei denen einer Schwierigkeiten hat,
werden dann von dem anderen unterstüzt.