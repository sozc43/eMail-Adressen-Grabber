Das Projekt heißt eMail-Adressen-Grabber und es handelt sich dabei umd ein
Programm, dass eMail-Adressen aus einem Text ausliest. Das Programm sollte 
eine eMail-Adresse erkennen und sie ausgeben. So wird es dem Nutzer erleichtert
mehrere, verschiedene eMail-Adressen aus einem langen Text rauszufiltern.
Die vermutlich größte technische Herausforderung ist es, dem Programm zu übermitteln,
wo eine eMail-Adresse beginnt und wo sie endet. Er sollte sie also nicht nur an dem
@ Zeichen erkennen. Außerdem sehen wir Schwierigkeiten darin, die Textdatei in 
das Programm einzulesen. Im folgenden sehen Sie, wie wir uns die Umsetzung vorgestellt
haben:

Lösungsansatz 
Um den eMail-Adressen-Grabber zu programmieren, wollen wir zuerst eine HTML Datei 
erstellen und diese mit der fopen Funktion in das Programm "hochladen".
Desweiteren, wollen wir die If - Else Bedingung verwenden. Dies soll folgendermaßen
aussehen:
If - wenn ein @ Zeichen vorhanden ist, soll er die Email Adresse ausgeben (printf)
Else - wenn kein @ Zeichen vorhanden ist, soll er ausgeben "keine Email Adresse vorhanden"